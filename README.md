# drupal

Content management platform. https://drupal.org/

## Official documentation
### Releases
* [*Drupal core release cycle: major, minor, and patch releases*
  ](https://www.drupal.org/core/release-cycle-overview)
* [*Drupal development roadmap*
  ](https://www.drupal.org/core/roadmap)

### Dependencies
* [*Drupal 9 now requires Symfony 4.4*
  ](https://www.drupal.org/node/3088712)

### Install
* [*Quick install for developers (command line)*
  ](https://www.drupal.org/documentation/install/developers)
* See also [php-packages-demo/drush](https://gitlab.com/php-packages-demo/drush)

### Testing
* [*Testing*](https://www.drupal.org/docs/8/testing) (2019)
* [*Running PHPUnit tests*
  ](https://www.drupal.org/docs/8/phpunit/running-phpunit-tests)
  2019-10

#### Automated  testing
* [*Automated tests*
  ](https://api.drupal.org/api/drupal/core!core.api.php/group/testing/8.2.x)
* [*Automated Testing*
  ](https://www.drupal.org/node/1449736) 2017-12
* [*Drupal automated test suite*
  ](https://www.drupal.org/node/24389)
  2007-11

## Unofficial documentation
### Testing
* [*Automated Testing in Drupal 8*
  ](https://drupalize.me/series/automated-testing-drupal-8)
  (2019) Paul Mitchum (Mile23 on drupal.org)

#### Search about testing
* [drupal module automated testing](https://google.com/search?q=drupal+module+automated+testing)
* [how can i run a drupal module test suite](https://google.com/search?q=how+can+i+run+a+drupal+module+test+suite)
